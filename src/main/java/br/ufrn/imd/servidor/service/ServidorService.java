package br.ufrn.imd.servidor.service;

import br.ufrn.imd.servidor.exceptions.BusinessRuleException;
import br.ufrn.imd.servidor.info.Info;
import br.ufrn.imd.servidor.retrofit.RepositoryInterface;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ServidorService {
    private final Map<Info, RepositoryInterface> repositorios = new HashMap<>();
    private final Random gerador = new Random();

    @Transactional
    public void addRepository(Info info, RepositoryInterface repo) {
        this.repositorios.put(info, repo);
    }

    public void add(String frase) throws BusinessRuleException, IOException {
        if (this.repositorios.size() < 1) {
            throw new BusinessRuleException("Não há repositorios");
        } else {
            for (String palavra : frase.split(" ")) {
                boolean palavraGuardada = false;
                while (!palavraGuardada) {
                    for (Info info : repositorios.keySet()) {
                        if (this.gerador.nextBoolean()) {
                            palavraGuardada = true;
                            RepositoryInterface repositorio = repositorios.get(info);
                            repositorio.save(palavra).execute();
                        }
                    }
                }
            }
        }
    }

    public String contains(String frase) throws IOException, BusinessRuleException {
        StringBuilder resposta = new StringBuilder();

        if (this.repositorios.isEmpty()) {
            throw new BusinessRuleException("Não há repositórios");
        }

        for (String palavra : frase.split(" ")) {
            List<String> repos = new ArrayList<>();
            for (Info key: repositorios.keySet()) {
                RepositoryInterface repositorio = repositorios.get(key);
                Boolean contains = repositorio.contains(palavra).execute().body();
                if (contains != null && contains) {
                    repos.add(key.getNome());
                }
            }
            if (repos.isEmpty()) {
                repos.add("Nenhum repositório");
            }
            resposta.append(String.join(", ", repos)).append(" guarda(m) a palavra ").append(palavra).append(".\n");
        }

        return resposta.toString();
    }
}
