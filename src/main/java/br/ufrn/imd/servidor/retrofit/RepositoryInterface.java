package br.ufrn.imd.servidor.retrofit;

import retrofit2.Call;
import retrofit2.http.*;

public interface RepositoryInterface {
    @GET("/api/palavra")
    Call<Boolean> contains(@Query("palavra") String palavra);

    @FormUrlEncoded
    @POST("/api/palavra")
    Call<Void> save(@Field("palavra") String palavra);
}
