package br.ufrn.imd.servidor.rest;

import br.ufrn.imd.servidor.exceptions.BusinessRuleException;
import br.ufrn.imd.servidor.info.Info;
import br.ufrn.imd.servidor.retrofit.RepositoryInterface;
import br.ufrn.imd.servidor.service.ServidorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

@RestController
@RequestMapping("/api/servidor")
@RequiredArgsConstructor
public class ServidorRestController {
    private final ServidorService servidorService;

    @PostMapping
    public boolean saveRepository(@RequestBody Info info) {
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://" + info.getEndereco() + ":" + info.getPorta())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RepositoryInterface service = retrofit.create(RepositoryInterface.class);

            this.servidorService.addRepository(info, service);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    @GetMapping(path = "/palavra")
    public String contains(@RequestParam("palavra") String palavra) throws IOException, BusinessRuleException {
        return this.servidorService.contains(palavra.strip());
    }

    @PostMapping(path = "/palavra")
    public void savePalavra(@RequestParam("palavra") String palavra) throws BusinessRuleException, IOException {
        this.servidorService.add(palavra.strip());
    }
}
