package br.ufrn.imd.servidor.info;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class Info implements Serializable {
    private String endereco;
    private String nome;
    private String porta;
}
