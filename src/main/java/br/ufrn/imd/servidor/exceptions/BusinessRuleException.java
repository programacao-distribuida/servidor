package br.ufrn.imd.servidor.exceptions;

public class BusinessRuleException extends Exception {
    public BusinessRuleException() {
    }

    public BusinessRuleException(String message) {
        super(message);
    }
}
